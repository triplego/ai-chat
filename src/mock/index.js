import Mock from 'mockjs';

const streamMock = Mock.mock('http://localhost:8080/stream', 'get', {
    status: 200,
    dataList: [1, 2, 2, 3, 3, 3, 4]
})

export default streamMock;