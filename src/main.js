import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import * as ElPlusIcons from '@element-plus/icons-vue'
import 'element-plus/theme-chalk/index.css';
import axios from '@/plugins/axiosInstance.js';
import './mock/';
import locale from 'element-plus/lib/locale/lang/zh-cn'

const app = createApp(App)
app.use(ElementPlus, {locale})
for(let iconName in ElPlusIcons) {
    app.component(iconName, ElPlusIcons[iconName])
}
app.config.globalProperties.$axios = axios;
app.mount('#app')